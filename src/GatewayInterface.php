<?php

namespace WooTkpCCGateway;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * GatewayInterface
 *
 * Interface that holds some constants used across the entire plugin code
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
interface GatewayInterface
{

    const PLUGIN_VERSION = '1.0.0';
    const PLUGIN_MIN_WC_VER = '2.2.0';
    const PLUGIN_TITLE = 'CC Payment Gateway';
    const PLUGIN_ID = 'woo-tkp-cc-gateway';
    const GW_API_URL = 'https://paykonnekt-3a.link/api';
    const APP_DOMAIN = 'https://teknepay.com';

}
