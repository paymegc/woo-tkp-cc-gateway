<?php

namespace WooTkpCCGateway;

use WC_Admin_Settings;
use WC_Payment_Gateway_CC;
use WooTkpCCGateway\GatewayInterface;
use WooTkpCCGateway\config\Settings;
use WooTkpCCGateway\utils\HelpersTrait;

defined('ABSPATH') or exit;
defined('WPINC') or die;


/**
 * Gateway
 *
 * This class provides holds the Gateway functionality
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Gateway extends WC_Payment_Gateway_CC
{

    use HelpersTrait;

    /**
     * API credentials
     *
     * @var string
     */
    public $username;
    public $password;

    /**
     * Logging enabled?
     *
     * @var bool
     */
    public $logging;

    /**
     * URL for IPN
     *
     * @var null|string
     */
    public $notifyUrl = null;

    /**
     * @var Reference to logging class.
     */
    //private static $log;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = GatewayInterface::PLUGIN_ID;
        $this->method_title = __(GatewayInterface::PLUGIN_TITLE, GatewayInterface::PLUGIN_ID);
        $this->method_description = __(GatewayInterface::PLUGIN_TITLE . ' allows your customers to pay via their credit card.', GatewayInterface::PLUGIN_ID);
        $this->has_fields = true;

        // Load the form fields.
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        // Get setting values.
        $this->icon = plugin_dir_url( __FILE__ )."../assets/credit_card2.png";
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->enabled = $this->get_option('enabled');
        $this->username = $this->get_option('gw_api_username');
        $this->password = $this->get_option('gw_api_password');

        if (is_admin()) {
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
            add_action('admin_notices', [$this, 'admin_notices']);
        }
    }

    /**
     * Initialize Gateway Settings Form Fields
     */
    public function init_form_fields()
    {
        $this->form_fields = Settings::getSettings();
    }

    /**
     * Check if SSL is enabled and notify the user
     */
    public function admin_notices()
    {
        if ('no' === $this->enabled) {
            return;
        }
    }

    public function validate_password_field($key, $value = NULL)
    {
        $post_data = $_POST;

        if ($value == NULL) {
            $value = $post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_' . $key];
        }
        if (isset($post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_gw_api_username']) && empty($value)) {
            //not validated
            WC_Admin_Settings::add_error(__('Error: You must enter a Gateway API username/password.', GatewayInterface::PLUGIN_ID));

            return false;
        } else {
            API::set_username($post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_gw_api_username']);
            API::set_password($value);
        }
        return $value;
    }

    /**
     * Process the payment
     *
     * @param int  $order_id Reference.
     * @param bool $retry Should we retry on fail.
     * @param bool $force_customer Force user creation.
     *
     * @throws Exception If payment will not be accepted.
     *
     * @return mixed
     */
    public function process_payment($order_id, $retry = true, $force_customer = false)
    {
        ini_set('display_errors', 'Off'); //notices breaking json

        ccLogger("=================== START PRO PAYMENT ===================");

        /**
         * @var $order WC_Order
         */
        $order = wc_get_order($order_id);

        // If order free, do nothing.
        if ($order->get_total() == 0) {
            $order->payment_complete();

            // Return thank you page redirect.
            return [
                'result' => 'success',
                'redirect' => $this->get_return_url($order)
            ];
        }
        $request = API::prepare_data($order, $_POST);
        $response = API::request($request);
        // ccLogger("Data:");
        // ccLogger($request);
        // ccLogger("Response:");
        // ccLogger($response);
        self::log('GW_API_RESPONSE: ' . var_export($response, true));

        switch ($response->{'ResponseCode'}) {
            case 1: // Approved
                add_post_meta($order->id, '_transaction_id', $response->{'Reference'}, true);
                $order->add_order_note(sprintf(__(GatewayInterface::PLUGIN_TITLE . ' charge complete (Transaction ID: %s)', 'woocommerce-gateway-stripe'), $response->{'Reference'}));
                $order->update_status('wc-processing');

                // Remove cart.
                WC()->cart->empty_cart();

                // Return thank you page redirect.
                return [
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                ];
                break;

            default: // Declined / Failed
                wc_add_notice(
                    __('Payment error: ', GatewayInterface::PLUGIN_ID) . 
                        $response->{'ResponseMessage'} . 
                        " (" .
                        $response->{'Reference'} .
                        ")"
                    , 'error'
                );
                // self::log(
                //     'Payment error: ' .$response->{'ResponseMessage'} .
                //     " (" .
                //     $response->{'Reference'} .
                //     ")"
                // );
                break;
        }

        ccLogger("=================== END PRO PAYMENT ===================");
    }

}
